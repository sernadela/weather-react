import React, { Component } from 'react';
import API from "../api";
import {WeatherInfo} from "./weather";


export default class MainPanel extends Component {

    constructor(props) {
        super(props);
        this.state = { city: '', cities: [], weather: []};
    }

    changeHandler = (event) => {
        event.preventDefault();
        this.setState({city: event.target.value});
    }

    handleReset = (event) => {
        event.preventDefault();
        this.setState( { city: '', cities: [], weather: []});
    }

    handleSubmit = event => {
        event.preventDefault();

        let cities = this.state.cities;
        if(!cities.includes(this.state.city)){
            cities.push(this.state.city);
        }
        this.setState({city:'', cities: cities});
        this.updateWeather();
    }

    updateWeather(){
        let cities = this.state.cities;
        let params = new URLSearchParams();
        for(let i=0; i < cities.length; i++){
            params.append("name", cities[i]);
        }

        API.get('/city/v1', {
            params: params
        }).then(res => {
            if (res.data.status !== 200) {
                alert(res.data.status + ':' + res.data.data.message);
                // remove bad city from state
                let c = this.state.cities;
                c.pop();
                this.setState({cities: c});
            } else {
                // parse weather data
                let req_data = res.data.data;
                let weather = []
                for (let i = 0; i < req_data.length; i++) {
                    let city = req_data[i].name;
                    let temp = parseFloat(req_data[i].temp);
                    let sunrise = new Date(parseInt(req_data[i].sunrise) * 1000).toLocaleTimeString();
                    let sunset = new Date(parseInt(req_data[i].sunset) * 1000).toLocaleTimeString();
                    weather.push({title: city, value: temp, sunrise: sunrise, sunset: sunset});
                }
                this.setState({weather: weather});
                //console.log(weather);
            }
        });
    }

    render() {
        return (
            <div>
                <React.StrictMode>
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col"></div>
                        <div className="col">
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label><h3>Weather APP</h3></label>
                                    <div className="form-group">
                                        <label>City Name:</label>
                                        <input type="text" value={this.state.city} onChange={this.changeHandler} className="form-control" placeholder="e.g. Aveiro" />
                                    </div>
                                    <button type="submit" disabled={!this.state.city} onSubmit={this.handleSubmit} className="btn btn-primary">Add City</button>
                                </div>
                            </form>
                        </div>
                        <div className="col"></div>
                    </div>
                    <div className="row align-items-center">
                        <div className="col">
                            <button type="submit" disabled={this.state.weather.length<=0} onClick={this.handleReset} className="btn btn-danger ">Reset</button>
                        </div>
                    </div>
                </div>
                </React.StrictMode>
                <WeatherInfo data={this.state.weather}/>
            </div>
        )
    }
}