import React, {Component} from "react";
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import {
    Chart,
    BarSeries,
    Title,
    ArgumentAxis,
    ValueAxis
} from '@devexpress/dx-react-chart-material-ui';

const columns = [{
    dataField: 'title',
    text: 'City',
    sort: true
}, {
    dataField: 'value',
    text: 'Temp(ºC)',
    sort: true
}, {
    dataField: 'sunrise',
    text: 'Sunrise',
    sort: true
},{
    dataField: 'sunset',
    text: 'Sunset',
    sort: true
}];

export class WeatherInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {data: []};
    }

    static getDerivedStateFromProps(props, state) {
        return {data: props.data };
    }

    render() {
        return (
            <div className={this.state.data.length > 0 ? "visible" : "invisible"}>
                <div className="container" >
                    <div className="row align-items-center">
                        <div className="col">
                            <Chart data={this.state.data}>
                                <ArgumentAxis />
                                <ValueAxis />
                                <BarSeries valueField="value" argumentField="title" />
                                <Title text="Temperature by city" />
                            </Chart>
                        </div>
                        <div className="col">
                            <BootstrapTable bootstrap4 keyField='title' data={this.state.data} columns={columns} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}