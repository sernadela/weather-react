import logo from './logo.svg';
import './App.css';
import MainPanel from './templates/main';



function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <div className="App-main">
          <MainPanel />
      </div>
    </div>
  );
}

export default App;
